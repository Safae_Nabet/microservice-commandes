FROM openjdk:8
EXPOSE 9001
ADD target/mcommandes-0.0.1-SNAPSHOT.jar mcommandes.jar
ENTRYPOINT ["java","-jar","/mcommandes.jar"]
